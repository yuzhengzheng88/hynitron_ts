ifneq ($(KERNELRELEASE),)
DRIVER_NAME := cst8xx


obj-m := $(DRIVER_NAME).o

$(DRIVER_NAME)-y += hyn_core.o

else

KERNELDIR ?= /lib/modules/$(shell uname -r)/build

PWD := $(shell pwd)

all:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

install: modules_install

modules_install:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules_install

clean:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) clean

endif
